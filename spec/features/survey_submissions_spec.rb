require 'rails_helper'

RSpec.feature "SurveySubmissions", type: :feature do
  let(:survey) { FactoryGirl.create(:full_survey) }
  let(:valid_attributes) { FactoryGirl.attributes_for(:survey_submission, survey: survey) }
  let(:openend_answer_attributes) { FactoryGirl.attributes_for(:survey_openend_answer) }

  def fill_money_questions_in_with(response)
    survey.survey_money_questions.count.times do | index |
      fill_in "survey_submission_survey_money_answers_attributes_#{index}_dollar",
      with: response
    end
  end
  def fill_rating_questions_in_with(response)
    survey.survey_rating_questions.count.times do | index |
      fill_in "survey_submission_survey_rating_answers_attributes_#{index}_value",
      with: response
    end
  end
  def fill_openend_questions_in_with(response)
    survey.survey_openend_questions.count.times do | index |
      fill_in "survey_submission_survey_openend_answers_attributes_#{index}_thought",
              with: response
    end
  end

  before { visit new_survey_survey_submission_path survey }

  describe "with valid survey information" do
    it "redirects to the survey's show page and displays the email" do

      fill_money_questions_in_with 2
      fill_rating_questions_in_with 5
      fill_openend_questions_in_with openend_answer_attributes[:thought]

      fill_in "Email", with: valid_attributes[:email]

      click_button "Submit"

      expect(page).to have_text /#{survey[:title]}/
      expect(page).to have_text /#{valid_attributes[:email]}/
      expect(page).to have_text /#{openend_answer_attributes[:thought]}/
      expect(page).to have_text "Thanks for your input"
      expect(current_path).to eq(survey_submission_path SurveySubmission.last )
    end
  end

  describe "with overly expensive survey information" do
    it "redirects to the survey's show page and displays the email" do
      fill_money_questions_in_with survey.max_budget
      fill_rating_questions_in_with 2
      fill_openend_questions_in_with openend_answer_attributes[:thought]

      fill_in "Email", with: valid_attributes[:email]

      click_button "Submit"

      expect(page).to have_text "Please review the problems below:"
      expect(page).to have_text "Your plan must cost less than the maximum budget"
      expect(page).not_to have_text "can't be blank"
      expect(page).not_to have_text "Thanks for your input"

      expect(current_path).to eq(survey_survey_submissions_path survey)
    end
  end

  describe "with incomplete survey information" do
    it "redirects to the survey's show page and displays the email" do

      click_button "Submit"

      expect(page).to have_text "can't be blank"
      expect(page).to have_text "Please review the problems below:"
      expect(page).not_to have_text "Thanks for your input"
      expect(current_path).to eq(survey_survey_submissions_path survey)
    end
  end
end
