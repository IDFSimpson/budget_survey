require 'rails_helper'

RSpec.describe SurveySubmissionsController, type: :controller do
  let(:survey) { FactoryGirl.create(:survey) }
  let(:survey_submission) { FactoryGirl.create(:survey_submission) }
  let(:user) { FactoryGirl.create(:user) }

  describe "#new" do
    before { get :new, survey_id: survey.id, survey_submission_id: survey_submission.id }

    it "renders the new template" do
      expect(response).to render_template(:new)
    end
    it "assigns a survey_submission object" do
      expect(assigns(:survey_submission)).to be_a_new(SurveySubmission)
    end
  end

  describe "#create" do
    context "with valid attributes" do
      before { sign_in user }
      let(:valid_request) { post :create, survey_id: survey.id, survey_submission: FactoryGirl.attributes_for(:survey_submission) }

      it "saves a record to the database" do
        count_before = SurveySubmission.count
        valid_request
        count_after = SurveySubmission.count
        expect(count_after).to eq(count_before + 1)
      end

      it "redirects to the survey submissions's show page" do
        valid_request
        expect(response).to redirect_to(survey_submission_path(SurveySubmission.last))
      end

      it "sets a flash message" do
        valid_request
        expect(flash[:notice]).to be
      end
    end

    context "with invalid attributes" do
      before { sign_in user }
      let(:invalid_request) do
        post :create, survey_id: survey.id,
              survey_submission: FactoryGirl.attributes_for(:survey_submission)
              .merge({email: nil})
      end

      it "renders the new template" do
        invalid_request
        expect(response).to render_template(:new)
      end

      it "doesn't save a record to the database" do
        count_before = SurveySubmission.count
        invalid_request
        count_after = SurveySubmission.count
        expect(count_after).to eq(count_before)
      end
    end

    describe "#show" do
      before do
        get :show, id: survey_submission.id
      end

      it "renders the show template" do
        expect(response).to render_template(:show)
      end

      it "sets a survey_submission instance variable" do
        expect(assigns(:survey_submission)).to eq(survey_submission)
      end
    end
  end
end
