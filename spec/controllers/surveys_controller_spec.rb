require 'rails_helper'

RSpec.describe SurveysController, type: :controller do
  let(:survey) { FactoryGirl.create(:full_survey) }
  let(:user) { FactoryGirl.create(:user) }

  describe "#new" do
    before { get :new, survey_id: survey.id }

    context "without a signed in user" do
      it "redirects to sign in" do
        expect(response).to redirect_to user_session_path
      end
    end
  end

  context "with signed in user" do
    before { sign_in user }
    it "renders the new template" do
     get :new, survey_id: survey.id
     expect(response).to render_template(:new)
    end
  end

  describe "#create" do
    context "with valid attributes" do
      before { sign_in user }
      let(:valid_request) { post :create, survey: FactoryGirl.attributes_for(:survey) }

      it "saves a record to the database" do
        count_before = Survey.count
        valid_request
        count_after = Survey.count
        expect(count_after).to eq(count_before + 1)
      end

      it "redirects to the survey's show page" do
        valid_request
        expect(response).to redirect_to(survey_path(Survey.last))
      end

      it "sets a flash message" do
        valid_request
        expect(flash[:notice]).to be
      end
    end

    context "with invalid attributes" do
      before { sign_in user }
      let(:invalid_request){ post :create, survey: {title: nil}}

      it "renders the new template" do
        invalid_request
        expect(response).to render_template(:new)
      end

      it "doesn't save a record to the database" do
        count_before = Survey.count
        invalid_request
        count_after = Survey.count
        expect(count_after).to eq(count_before)
      end
    end
  end

  describe "#update" do
    before { sign_in user }

    context "#with a valid budget question" do
      it "makes sure a user can add a budget question" do
        valid_question = FactoryGirl.attributes_for(:survey_money_question, survey_id: survey.id)
        post :update, id: survey.id, survey: { survey_money_questions_attributes: [ valid_question ] }
        survey.reload
        new_question = survey.survey_questions.last
        expect(new_question.body).to eq(valid_question[:body])
      end
      it "makes sure a user can edit a budget question" do
        valid_question = FactoryGirl.attributes_for(:survey_money_question, survey_id: survey.id)
        post :update, id: survey.id, survey: { survey_money_questions_attributes: [ valid_question ] }
        survey.reload
        new_question = survey.survey_questions.last
        expect(new_question.body).to eq(valid_question[:body])
      end
    end
    context "#with some valid questions" do
      let(:valid_openend_question) do
        FactoryGirl.attributes_for(:survey_openend_question, survey_id: survey.id)
      end
      let(:openend_thought) { FactoryGirl.attributes_for(:survey_openend_question)[:body] }
      before do
        post :update, id: survey.id, survey: { survey_openend_questions_attributes:
                                               [ valid_openend_question ] }
      end
      it "makes sure a user can add a open-end question" do
        survey.reload
        new_question = survey.survey_questions.last
        expect(new_question.body).to eq(valid_openend_question[:body])
      end
      it "makes sure a user can edit a open-end question" do
        valid_openend_question[:body] = openend_thought
        post :update, id: survey.id, survey: { survey_openend_questions_attributes: [ valid_openend_question ] }
        edited_question = survey.survey_questions.last
        expect(edited_question.body).to eq(openend_thought)
      end
    end
    context "#with invalid questions" do
      let(:invalid_attributes) { FactoryGirl.attributes_for(:invalid_survey_question, survey_id: survey.id) }

      pending "makes sure a user can't add budget question" do
        expect do
          post :update, id: survey.id, survey: { survey_money_questions_attributes: [ invalid_attributes ] }
          survey.reload
        end.not_to change(SurveyQuestion, :count)
      end
      pending "makes sure a user can't add an open-end question" do
        expect do
          post :update, id: survey.id, survey: { survey_openend_questions_attributes: [ invalid_attributes ] }
          survey.reload
          new_question = survey.survey_questions.last
          expect(new_question.body).not_to eq(invalid_attributes[:body])
        end.not_to change(SurveyQuestion, :count)
      end
    end
  end
end
