require 'rails_helper'

RSpec.describe SurveyMoneyAnswer, type: :model do
  describe "validations" do
    it "requires an integer for the dollar value" do
      a = FactoryGirl.build(:survey_money_answer, dollar: "-0.1")
      expect(a.valid?).to eq(false)
    end
    it "requires input to conform to ticks" do
      qu = FactoryGirl.build(:survey_money_question)
      tick = 10
      qu.survey_question_ticks.build value: tick, label: "foo"
      qu.save
      qu.survey_money_answers << FactoryGirl.build(:survey_money_answer, dollar: (tick))
      expect(qu.valid?).to eq(true)
      qu.survey_money_answers << FactoryGirl.build(:survey_money_answer, dollar: (tick + 1))
      expect(qu.valid?).to eq(false)
    end
  end
end
