require 'rails_helper'

RSpec.describe SurveySubmission, type: :model do
  describe "validations" do
    it "requires submission to cost less than the preset budget" do
      f = FactoryGirl.create(:full_survey)
      f.max_budget = f.average_spending.floor - 1
      expect(f.survey_submissions.first).to be_invalid
    end
  end

  describe "data summation" do
    context "with valid answers" do
      let (:survey) { FactoryGirl.create(:full_survey) }

      it "returns radar chart gem json for spending" do
        ss = survey.survey_submissions.first
        q = survey.survey_questions.last
        data = JSON.parse(ss.chart_money_datum)

        expect(survey.chart_average_dollar_data).to be_an(String)
        expect(data["labels"].last).to eq(q.title)
        expect(data["datasets"].first["label"]).to be_an(String)
        expect(data["datasets"].first["data"].last.to_f).
                  to eq(q.survey_money_answers.average(:dollar))
      end
      it "returns average submission spending" do
        ss = survey.survey_submissions.first
        expect(ss.submission_spending).to eq(ss.survey_money_answers.sum(:dollar))
      end
    end

    context "with out answers" do
      it "returns 0 for average spending" do
        ss = FactoryGirl.create(:survey_submission)
        expect(ss.submission_spending).to eq(0)
      end
    end
  end
end
