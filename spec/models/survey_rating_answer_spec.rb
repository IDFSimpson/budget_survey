require 'rails_helper'

RSpec.describe SurveyRatingAnswer, type: :model do
  describe "validations" do
    it "requires an integer for the value value" do
      a = FactoryGirl.build(:survey_rating_answer, value: "-0.1")
      expect(a.valid?).to eq(false)
    end
    it "requires input to conform to ticks" do
      qu = FactoryGirl.build(:survey_rating_question)
      tick = 10
      qu.survey_question_ticks.build value: tick, label: "foo"
      qu.save
      qu.survey_rating_answers << FactoryGirl.build(:survey_rating_answer, value: (tick))
      expect(qu.valid?).to eq(true)
      qu.survey_rating_answers << FactoryGirl.build(:survey_rating_answer, value: (tick + 1))
      expect(qu.valid?).to eq(false)
    end
  end
end
