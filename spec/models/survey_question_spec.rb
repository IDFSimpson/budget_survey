require 'rails_helper'

RSpec.describe SurveyQuestion, type: :model do
  pending "must have a max is greater than min" do
    q = FactoryGirl.build(:survey_question, {max: 1, min: 10})
    expect(q).to be_invalid
  end
end
