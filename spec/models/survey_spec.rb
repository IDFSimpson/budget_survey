require 'rails_helper'

RSpec.describe Survey, type: :model do
  describe "validations" do
    it "requires a title" do
      s = FactoryGirl.build(:survey, title: nil)
      expect(s.valid?).to eq(false)
    end
    it "requires an integer for the maximum budget" do
      a = FactoryGirl.build(:survey, max_budget: "",
                            survey_money_questions:
                              [ FactoryGirl.create(:survey_money_question) ] )
      expect(a.valid?).to eq(false)
    end
    it "requires questions to also be valid" do
      a = FactoryGirl.build(:survey,
                            survey_money_questions_attributes:
                                [ FactoryGirl.attributes_for(:invalid_survey_question)] )
      expect(a).to be_invalid
      expect(a.errors.messages[:survey_money_questions]).to eq ['is invalid']
      expect(a.errors.messages[:"survey_money_questions.title"]).to eq ["can't be blank"]
      expect(a.errors.messages[:"survey_money_questions.min"]).to eq ["You must add either a range or tick values"]
    end
  end

  describe "data summation" do
    context "with valid survey answers" do
      let (:survey) do
        FactoryGirl.create(:full_survey)
      end

      it "returns chart gem json data for a histogram" do
        data = JSON.parse(survey.chart_histogram_data.values.last)

        expect(survey.chart_histogram_data.values.last).to be_an(String)
        expect(data["labels"].last).to be_an(Numeric)
        expect(data["datasets"].first["label"].last).to be_an(Numeric)
        expect(data["datasets"].first["data"].last).to be_an(Numeric)
      end
      it "returns chart gem radar json" do
        q = survey.survey_questions.last
        data = JSON.parse(survey.chart_average_dollar_data)

        expect(survey.chart_average_dollar_data).to be_an(String)
        expect(data["labels"].last).to eq(q.title)
        expect(data["datasets"].first["label"].last).to be_an(String)
        expect(data["datasets"].first["data"].last.to_f).to eq(q.survey_money_answers.average(:dollar))
      end
      it "returns an integer average participant spending" do
        q = FactoryGirl.create(:survey_money_question_with_answer)
        expect(q.survey.average_spending).to eq(q.survey_money_answers.first.dollar)
      end
    end
  end
  context "without survey answers" do
    let (:survey) { survey = FactoryGirl.create(:survey) }

    it "returns empty chart gem json data for a radar chart" do
      expect(survey.chart_histogram_data).to eq({})
    end
    it "returns empty chart gem json data for a histogram" do
      expect(survey.chart_histogram_data).to eq({})
    end
    pending "returns an 0 for average participant spending" do
      expect(survey.average_spending).to eq(0)
    end
  end
end
