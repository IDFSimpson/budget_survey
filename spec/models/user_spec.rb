require 'rails_helper'

RSpec.describe User, type: :model do
  describe "validations" do
      it "requires an email" do
        u = User.new FactoryGirl.attributes_for(:user).merge({email: nil})
        expect(u).to be_invalid
      end

      it "requires a unique email" do
        u  = FactoryGirl.create(:user)
        u2 = User.new FactoryGirl.attributes_for(:user).merge({email: u.email})
        expect(u2).to be_invalid
      end
    end

    describe "hashing the password" do
      it "generates a password digest" do
        u = User.new FactoryGirl.attributes_for(:user)
        u.save
        expect(u.encrypted_password).to be
      end
  end
end
