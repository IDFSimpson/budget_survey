FactoryGirl.define do
  factory :survey_question do
    association :survey, factory: :survey

    title       { Faker::Book.title }
    body        { Faker::Lorem.paragraph }
    min         { rand(100) }
    max         { 100 + rand(14999900) }
    step        { rand(100) }
    type        "SurveyMoneyQuestion"

    factory :invalid_survey_question do
      title     nil
      step      nil
      min       nil
    end
  end

  factory :survey_money_question do
    association :survey, factory: :survey

    title       { Faker::Book.title }
    body        { Faker::Lorem.paragraph }
    min         { rand(100) }
    max         { 100 + rand(14999900) }
    step        { rand(100) }

    factory :survey_money_question_with_answer do
      transient do
        survey_submission { create(:survey_submission) }
      end
      after(:create) do |survey_money_question, evaluator|
        survey_money_question.survey_money_answers.create(
                        attributes_for(:survey_money_answer)
                          .merge({survey_submission: evaluator.survey_submission}))
        survey_money_question.survey.survey_submissions << evaluator.survey_submission
        survey_money_question.survey.reload
      end
    end
  end

  factory :survey_openend_question do
    association :survey, factory: :survey

    title       { Faker::Book.title }
    body        { Faker::Lorem.paragraph }

    factory :survey_openend_question_with_answer do
      transient do
        survey_submission { create(:survey_submission) }
      end
      after(:create) do |survey_openend_question, evaluator|
        survey_openend_question.survey_openend_answers.create(
          attributes_for(:survey_openend_answer)
          .merge({survey_submission: evaluator.survey_submission}))
          survey_openend_question.survey.survey_submissions << evaluator.survey_submission
          survey_openend_question.survey.reload
      end
    end
  end

  factory :survey_rating_question do
    association :survey, factory: :survey

    title       { Faker::Book.title }
    body        { Faker::Lorem.paragraph }
    min         { rand(100) }
    max         { 100 + rand(14999900) }
    step        { rand(100) }

    factory :survey_rating_question_with_answer do
      transient do
        survey_submission { create(:survey_submission) }
      end
      after(:create) do |survey_rating_question, evaluator|
        survey_rating_question.survey_rating_answers.create(
                        attributes_for(:survey_rating_answer)
                          .merge({survey_submission: evaluator.survey_submission}))
        survey_rating_question.survey.survey_submissions << evaluator.survey_submission
        survey_rating_question.survey.reload
      end
    end
  end
end
