FactoryGirl.define do
  factory :survey do
    association :user, factory: :user
    sequence(:title)  { |n| "#{Faker::Book.title}-#{n}"}
    max_budget        { rand(15000000) }

    factory :survey_with_questions do
      transient do
        survey_questions_count 2
      end

      after(:create) do |survey, evaluator|
        create_list(:survey_openend_question, evaluator.survey_questions_count, survey: survey)
        create_list(:survey_rating_question, evaluator.survey_questions_count, survey: survey)
        create_list(:survey_money_question, evaluator.survey_questions_count, survey: survey)
      end
    end

    factory :full_survey do
      transient do
        survey_questions_count 2
        survey_submission { create(:survey_submission) }
      end

      after(:create) do |survey, evaluator|
        create_list(:survey_openend_question_with_answer, evaluator.survey_questions_count,
                    {survey: survey, survey_submission: evaluator.survey_submission})
        create_list(:survey_rating_question_with_answer, evaluator.survey_questions_count,
                    {survey: survey, survey_submission: evaluator.survey_submission})
        create_list(:survey_money_question_with_answer, evaluator.survey_questions_count,
                    {survey: survey, survey_submission: evaluator.survey_submission})
        survey.reload
      end
    end
  end
end
