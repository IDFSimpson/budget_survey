FactoryGirl.define do
  factory :survey_rating_answer do
    association :survey_submission, factory: :survey_submission
    association :survey_rating_question, factory: :survey_rating_question

    value 1
  end
end
