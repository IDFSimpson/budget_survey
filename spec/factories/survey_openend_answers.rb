FactoryGirl.define do
  factory :survey_openend_answer do
    association :survey_submission, factory: :survey_submission
    association :survey_openend_question, factory: :survey_openend_question

    thought  { Faker::Lorem.paragraph }
  end
end
