FactoryGirl.define do
  factory :survey_submission do
    association :survey, factory: :survey

    sequence(:email)      { |n| Faker::Internet.email.gsub("@", "-#{n}@") }
  end
end
