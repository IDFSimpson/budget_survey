FactoryGirl.define do
  factory :survey_money_answer do
    association :survey_submission, factory: :survey_submission
    association :survey_money_question, factory: :survey_money_question

    dollar  { rand(15000000)}
  end
end
