class AddEmailIdToSurveySubmission < ActiveRecord::Migration
  def change
    add_column :survey_submissions, :email, :string
  end
end
