class AddStiTypeToSurveyQuestions < ActiveRecord::Migration
  def up
    add_column :survey_questions, :type, :string
    execute "update survey_questions set type = 'SurveyMoneyQuestion'"
    rename_column :survey_money_answers, :survey_question_id, :survey_money_question_id
  end

  def down
    remove_column :survey_questions, :type, :string
    rename_column :survey_money_answers, :survey_money_question_id, :survey_question_id
  end
end
