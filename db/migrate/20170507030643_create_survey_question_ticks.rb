class CreateSurveyQuestionTicks < ActiveRecord::Migration
  def change
    create_table :survey_question_ticks do |t|
      t.integer :value
      t.string :label
      t.references :survey_question, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
