class CreateSurveyRatingAnswers < ActiveRecord::Migration
  def change
    create_table :survey_rating_answers do |t|
      t.integer :value
      t.references :survey_rating_question, index: true, foreign_key: false
      t.references :survey_submission, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
