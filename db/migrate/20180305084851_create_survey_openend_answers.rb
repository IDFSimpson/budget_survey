class CreateSurveyOpenendAnswers < ActiveRecord::Migration
  def change
    create_table :survey_openend_answers do |t|
      t.text :thought
      t.references :survey_openend_question, index: true, foreign_key: false
      t.references :survey_submission, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
