class AddTitleToSurveyQuestions < ActiveRecord::Migration
  def change
    add_column :survey_questions, :title, :string
  end
end
