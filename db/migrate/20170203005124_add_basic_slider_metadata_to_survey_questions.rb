class AddBasicSliderMetadataToSurveyQuestions < ActiveRecord::Migration
  def change
    add_column :survey_questions, :min, :integer
    add_column :survey_questions, :max, :integer
    add_column :survey_questions, :step, :integer
  end
end
