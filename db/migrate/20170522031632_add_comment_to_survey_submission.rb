class AddCommentToSurveySubmission < ActiveRecord::Migration
  def change
    add_column :survey_submissions, :comment, :text
  end
end
