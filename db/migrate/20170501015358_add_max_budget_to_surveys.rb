class AddMaxBudgetToSurveys < ActiveRecord::Migration
  def change
    add_column :surveys, :max_budget, :integer
  end
end
