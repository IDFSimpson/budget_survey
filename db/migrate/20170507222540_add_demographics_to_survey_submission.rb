class AddDemographicsToSurveySubmission < ActiveRecord::Migration
  def change
    add_column :survey_submissions, :age_range, :string
    add_column :survey_submissions, :is_local, :boolean
  end
end
