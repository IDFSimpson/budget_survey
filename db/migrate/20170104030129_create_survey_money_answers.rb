class CreateSurveyMoneyAnswers < ActiveRecord::Migration
  def change
    create_table :survey_money_answers do |t|
      t.integer :dollar
      t.references :survey_question, index: true, foreign_key: true
      t.references :survey_submission, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
