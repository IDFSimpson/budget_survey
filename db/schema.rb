# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180308085236) do

  create_table "survey_money_answers", force: :cascade do |t|
    t.integer  "dollar",                   limit: 4
    t.integer  "survey_money_question_id", limit: 4
    t.integer  "survey_submission_id",     limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "survey_money_answers", ["survey_money_question_id"], name: "index_survey_money_answers_on_survey_money_question_id", using: :btree
  add_index "survey_money_answers", ["survey_submission_id"], name: "index_survey_money_answers_on_survey_submission_id", using: :btree

  create_table "survey_openend_answers", force: :cascade do |t|
    t.text     "thought",                    limit: 65535
    t.integer  "survey_openend_question_id", limit: 4
    t.integer  "survey_submission_id",       limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "survey_openend_answers", ["survey_openend_question_id"], name: "index_survey_openend_answers_on_survey_openend_question_id", using: :btree
  add_index "survey_openend_answers", ["survey_submission_id"], name: "index_survey_openend_answers_on_survey_submission_id", using: :btree

  create_table "survey_question_ticks", force: :cascade do |t|
    t.integer  "value",              limit: 4
    t.string   "label",              limit: 255
    t.integer  "survey_question_id", limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "survey_question_ticks", ["survey_question_id"], name: "index_survey_question_ticks_on_survey_question_id", using: :btree

  create_table "survey_questions", force: :cascade do |t|
    t.text     "body",       limit: 65535
    t.integer  "survey_id",  limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "min",        limit: 4
    t.integer  "max",        limit: 4
    t.integer  "step",       limit: 4
    t.string   "title",      limit: 255
    t.string   "type",       limit: 255
  end

  add_index "survey_questions", ["survey_id"], name: "index_survey_questions_on_survey_id", using: :btree

  create_table "survey_rating_answers", force: :cascade do |t|
    t.integer  "value",                     limit: 4
    t.integer  "survey_rating_question_id", limit: 4
    t.integer  "survey_submission_id",      limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "survey_rating_answers", ["survey_rating_question_id"], name: "index_survey_rating_answers_on_survey_rating_question_id", using: :btree
  add_index "survey_rating_answers", ["survey_submission_id"], name: "index_survey_rating_answers_on_survey_submission_id", using: :btree

  create_table "survey_submissions", force: :cascade do |t|
    t.integer  "survey_id",  limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "email",      limit: 255
    t.string   "age_range",  limit: 255
    t.boolean  "is_local"
    t.text     "comment",    limit: 65535
  end

  add_index "survey_submissions", ["survey_id"], name: "index_survey_submissions_on_survey_id", using: :btree

  create_table "surveys", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "max_budget", limit: 4
  end

  add_index "surveys", ["user_id"], name: "index_surveys_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "survey_money_answers", "survey_questions", column: "survey_money_question_id"
  add_foreign_key "survey_money_answers", "survey_submissions"
  add_foreign_key "survey_openend_answers", "survey_submissions"
  add_foreign_key "survey_question_ticks", "survey_questions"
  add_foreign_key "survey_questions", "surveys"
  add_foreign_key "survey_rating_answers", "survey_submissions"
  add_foreign_key "survey_submissions", "surveys"
  add_foreign_key "surveys", "users"
end
