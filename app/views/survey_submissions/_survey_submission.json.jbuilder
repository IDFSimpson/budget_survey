json.extract! survey_submission, :id, :survey_id, :created_at, :updated_at
json.url survey_submission_url(survey_submission, format: :json)
