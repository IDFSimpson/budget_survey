json.extract! survey_money_answer, :id, :created_at, :updated_at
json.url survey_money_answer_url(survey_money_answer, format: :json)