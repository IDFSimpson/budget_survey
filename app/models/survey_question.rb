class SurveyQuestion < ActiveRecord::Base
  belongs_to :survey
  has_many :survey_question_ticks, dependent: :destroy

  accepts_nested_attributes_for :survey_question_ticks,
                                reject_if: :all_blank,
                                allow_destroy: true


  validates :title, presence: true
  validates :body, presence: true
  validates :survey, presence: true

  def answers_type
    @answers_type ||= type.gsub(/Question/, "Answer").underscore.pluralize.to_sym
  end

  def question_type
    @question_type ||= type.underscore.to_sym
  end
end
