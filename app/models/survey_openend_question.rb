class SurveyOpenendQuestion < SurveyQuestion
  belongs_to :survey, inverse_of: :survey_openend_questions
  has_many :survey_openend_answers, dependent: :destroy

  validates :survey, presence: true
end
