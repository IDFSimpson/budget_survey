class SurveySubmission < ActiveRecord::Base
  belongs_to :survey
  has_many :survey_money_answers,   dependent: :destroy, inverse_of: :survey_submission
  has_many :survey_rating_answers,  dependent: :destroy, inverse_of: :survey_submission
  has_many :survey_openend_answers, dependent: :destroy, inverse_of: :survey_submission
  has_many :survey_questions,        through: :survey
  has_many :survey_money_questions,  through: :survey
  has_many :survey_rating_questions, through: :survey

  validates_associated :survey_money_answers,
                       :survey_rating_answers,
                       :survey_openend_answers

  accepts_nested_attributes_for :survey_money_answers,
                                :survey_openend_answers,
                                :survey_rating_answers,
                                reject_if: :all_blank,
                                allow_destroy: true

  validates :survey, presence: true

  validate :submission_costs_less_than_max_budget

  VALID_EMAIL_REGEX = /\A([\w+\-]\.?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :email, presence: true, uniqueness: {scope: :survey}, format: VALID_EMAIL_REGEX

  def submission_spending
    begin
      self.survey_money_answers.map(&:dollar).sum
    rescue
      0
    end
  end

  def submission_avg_rating
    begin
      self.survey_rating_answers.average(:value)
    rescue
      0
    end
  end

  def chart_money_datum
    {
      labels: self.survey_money_questions.pluck(:title),
      datasets:[
          {
              label: "Submissions Average",
              fillColor: "rgba(179,181,198,0.2)",
              strokeColor: "rgba(179,181,198,1)",
              pointColor: "rgba(179,181,198,1)",
              pointStrokeColor: "#fff",
              pointHightlightFill: "#fff",
              pointHightlightStroke: "rgba(179,181,198,1)",
              data: self.survey_money_answers.pluck(:dollar)
          }
      ]
    }.to_json
  end

  private

    def submission_costs_less_than_max_budget
      if self.submission_spending > self.survey.max_budget
        errors.add(:spending, "Your plan must cost less than the maximum budget")
      end
    end

end
