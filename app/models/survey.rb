class Survey < ActiveRecord::Base
  require 'histogram/array'  # enables Array#histogram
  HISTOGRAM_BIN_SIZE = 5

  belongs_to :user
  has_many :survey_questions, dependent: :destroy, inverse_of: :survey
  has_many :survey_submissions, dependent: :destroy, inverse_of: :survey
  has_many :survey_money_questions, dependent: :destroy, inverse_of: :survey
  has_many :survey_money_answers, through: :survey_submissions
  has_many :survey_rating_questions, dependent: :destroy, inverse_of: :survey
  has_many :survey_rating_answers, through: :survey_submissions
  has_many :survey_openend_questions, dependent: :destroy, inverse_of: :survey
  has_many :survey_openend_answers,   through: :survey_submissions

  validates_associated          :survey_money_questions,
                                :survey_openend_questions,
                                :survey_questions

  accepts_nested_attributes_for :survey_money_questions,
                                :survey_openend_questions,
                                :survey_rating_questions,
                                reject_if: :all_blank,
                                allow_destroy: true

  validates :title, presence: true
  validates :user, presence: true
  validates :max_budget, numericality: { only_integer: true }, allow_nil: true
  validate :max_budget_if_questions_exist

  def budgeting?
    self.survey_money_answers.any?
  end

  def rating?
    self.survey_rating_answers.any?
  end

  def range_values_for(question)
    question.try(:survey_money_answers).try(:pluck, :dollar) ||
      question.try(:survey_rating_answers).try(:pluck, :value)
  end

  def chart_histogram_data
    histogram = {}
    self.survey_questions.where("type = 'SurveyMoneyQuestion' or type = 'SurveyRatingQuestion'").each do |qu|
      chart_data = range_values_for qu
      (bins, freqs) = chart_data.histogram(HISTOGRAM_BIN_SIZE)
      begin
        bins.map!(&:truncate)
      rescue
        puts "The chart labels could not be truncated. Check for nils in answers for survey #{self.id}"
      end
      histogram[qu] = chart_datum(labels: bins, label: bins, data: freqs).to_json
    end
    histogram
  end

  def chart_average_dollar_data
    question_labels = []
    average_data = []
    self.survey_money_questions.each do |qu|
      question_labels << qu.title
      average_data << qu.survey_money_answers.average(:dollar)
    end
    chart_datum(labels: question_labels, data: average_data).to_json
  end

  def chart_average_rating_data
    question_labels = []
    average_data = []
    self.survey_rating_questions.each do |qu|
      question_labels << qu.title
      average_data << qu.survey_rating_answers.average(:value)
    end
    chart_datum(labels: question_labels, data: average_data).to_json
  end

  def average_spending
    self.survey_money_answers.average(:dollar) || nil
  end

  def average_rating
    self.survey_money_answers.average(:dollar) || nil
  end

  def AGE_RANGE_CHOICES
    [ "< 18", "18 – 24", "25 – 35", "35 – 44", "44 – 54", "> 55" ]
  end

  private

    def chart_datum(labels: nil, data: nil, label: "Submissions Average")
      {
        labels: labels,
        datasets:[
            {
                label: label,
                fillColor: "rgba(179,181,198,0.2)",
                strokeColor: "rgba(179,181,198,1)",
                pointColor: "rgba(179,181,198,1)",
                pointStrokeColor: "#fff",
                pointHightlightFill: "#fff",
                pointHightlightStroke: "rgba(179,181,198,1)",
                data: data
            }
        ]
      }
    end

    def max_budget_if_questions_exist
      if self.max_budget.nil? && self.survey_money_questions.any?
        errors.add(:max_budget, "Including budget questions requires a maximum to be set")
      end
    end

end
