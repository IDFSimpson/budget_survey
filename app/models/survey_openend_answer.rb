class SurveyOpenendAnswer < ActiveRecord::Base
  belongs_to :survey_submission
  belongs_to :survey_question, foreign_key: 'survey_openend_question_id'
  belongs_to :survey_openend_question

  validates :survey_submission, presence: true
  validates :survey_openend_question, presence: true
end
