class SurveyQuestionTick < ActiveRecord::Base
  belongs_to :survey_question
  belongs_to :survey_money_question, foreign_key: 'survey_question_id'
  belongs_to :survey_rating_question, foreign_key: 'survey_question_id'

  validates :value, numericality: { only_integer: true }, presence: true
end
