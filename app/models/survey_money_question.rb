class SurveyMoneyQuestion < SurveyQuestion
  has_many :survey_money_answers, dependent: :destroy
  belongs_to :survey

  validates_associated :survey_question_ticks

  validates :survey, presence: true
  validate :has_steps_or_ticks

  def min_value
    self.survey_question_ticks.pluck(:value).first || self.min
  end

  def max_value
    self.survey_question_ticks.pluck(:value).last || self.max
  end

  def tick(column)
    survey_question_ticks.pluck(column).to_s if survey_question_ticks
  end

  private
    def has_steps_or_ticks
      unless (!self.min.nil? && !self.max.nil? && !self.step.nil? ) || self.survey_question_ticks.any?
        errors.add(:min, "You must add either a range or tick values") if self.min.nil?
        errors.add(:max, "You must add either a range or tick values") if self.max.nil?
      end
    end
end
