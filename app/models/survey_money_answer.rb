class SurveyMoneyAnswer < ActiveRecord::Base
  belongs_to :survey_question, foreign_key: 'survey_money_question_id'
  belongs_to :survey_money_question
  belongs_to :survey_submission

  validates :dollar, presence: true, numericality: { only_integer: true }
  validate :validate_in_question_ticks
  validates :survey_money_question, presence: true
  validates :survey_submission, presence: true

  private
    def validate_in_question_ticks
      if self.survey_money_question.survey_question_ticks.size > 0 &&
        !(self.survey_money_question.survey_question_ticks.pluck(:value).include? self.dollar)
        errors.add(:dollar, 'slider head must be on a tick')
      end
    end
end
