class SurveyRatingAnswer < ActiveRecord::Base
  belongs_to :survey_submission
  belongs_to :survey_question, foreign_key: 'survey_rating_question_id'
  belongs_to :survey_rating_question

  validates :value, presence: true, numericality: { only_integer: true }
  validate :validate_in_question_ticks
  validates :survey_rating_question, presence: true
  validates :survey_submission, presence: true

  private
    def validate_in_question_ticks
      if self.survey_rating_question.survey_question_ticks.size > 0 &&
        !(self.survey_rating_question.survey_question_ticks.pluck(:value).include? self.value)
        errors.add(:value, 'slider head must be on a tick')
      end
    end
end
