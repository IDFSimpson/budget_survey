class SurveySubmissionsController < ApplicationController
  before_action :authenticate_user!, except: [:new, :create, :show]
  before_action :set_survey_submission, only: [:show, :edit, :update, :destroy]
  before_action :set_survey, only: [:new, :create, :index]

  # GET /survey_submissions
  # GET /survey_submissions.json
  def index
    @survey_submissions = SurveySubmission.where(survey: params["survey_id"])

    @answers_histogram_data = @survey.chart_histogram_data
    @radar_data = @survey.chart_average_dollar_data
    @chart_rating_average = @survey.chart_average_rating_data
  end

  # GET /survey_submissions/1
  # GET /survey_submissions/1.json
  def show
  end

  # GET /survey_submissions/new
  def new
    @survey_submission = SurveySubmission.new
    @survey_submission.survey = @survey
    @survey.survey_questions.each do |qu|
      @survey_submission.public_send(qu.answers_type)
                        .build("#{qu.question_type}_id".to_sym => qu.id)
    end
  end

  # GET /survey_submissions/1/edit
  def edit
  end

  # POST /survey_submissions
  # POST /survey_submissions.json
  def create
    @survey_submission = SurveySubmission.new(survey_submission_params)
    @survey_submission.survey = @survey

    respond_to do |format|
      if @survey_submission.save
        format.html { redirect_to @survey_submission, notice: 'Thanks for your input!' }
        format.json { render :show, status: :created, location: @survey_submission }
      else
        format.html { render :new }
        format.json { render json: @survey_submission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /survey_submissions/1
  # PATCH/PUT /survey_submissions/1.json
  def update
    respond_to do |format|
      if @survey_submission.update(survey_submission_params)
        format.html { redirect_to @survey_submission, notice: 'Survey submission was successfully updated.' }
        format.json { render :show, status: :ok, location: @survey_submission }
      else
        format.html { render :edit }
        format.json { render json: @survey_submission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /survey_submissions/1
  # DELETE /survey_submissions/1.json
  def destroy
    @survey_submission.destroy
    respond_to do |format|
      format.html { redirect_to survey_survey_submissions_path @survey_submission.survey, notice: 'Survey submission was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_survey_submission
      @survey_submission = SurveySubmission.find(params[:id])
    end

    def set_survey
      @survey = Survey.find(params[:survey_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def survey_submission_params
      params.require(:survey_submission).permit(:email,
                                                :is_local,
                                                :age_range,
                                                :comment,
                                                survey_money_answers_attributes: [
                                                            :dollar,
                                                            :id,
                                                            :survey_money_question_id,
                                                            :_destroy ],
                                                survey_rating_answers_attributes: [
                                                            :value,
                                                            :id,
                                                            :survey_rating_question_id,
                                                            :_destroy ],
                                                survey_openend_answers_attributes: [
                                                            :thought,
                                                            :id,
                                                            :survey_openend_question_id,
                                                            :_destroy ] )
    end
end
