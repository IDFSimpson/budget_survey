class SurveyMoneyAnswersController < ApplicationController
  before_action :set_survey_money_answer, only: [:show, :edit, :update, :destroy]

  # GET /survey_money_answers
  # GET /survey_money_answers.json
  def index
    @survey_money_answers = SurveyMoneyAnswer.all
  end

  # GET /survey_money_answers/1
  # GET /survey_money_answers/1.json
  def show
  end

  # GET /survey_money_answers/new
  def new
    @survey_money_answer = SurveyMoneyAnswer.new
  end

  # GET /survey_money_answers/1/edit
  def edit
  end

  # POST /survey_money_answers
  # POST /survey_money_answers.json
  def create
    @survey_money_answer = SurveyMoneyAnswer.new(survey_money_answer_params)

    respond_to do |format|
      if @survey_money_answer.save
        format.html { redirect_to @survey_money_answer, notice: 'Survey answer was successfully created.' }
        format.json { render :show, status: :created, location: @survey_money_answer }
      else
        format.html { render :new }
        format.json { render json: @survey_money_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /survey_money_answers/1
  # PATCH/PUT /survey_money_answers/1.json
  def update
    respond_to do |format|
      if @survey_money_answer.update(survey_money_answer_params)
        format.html { redirect_to @survey_money_answer, notice: 'Survey answer was successfully updated.' }
        format.json { render :show, status: :ok, location: @survey_money_answer }
      else
        format.html { render :edit }
        format.json { render json: @survey_money_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /survey_money_answers/1
  # DELETE /survey_money_answers/1.json
  def destroy
    @survey_money_answer.destroy
    respond_to do |format|
      format.html { redirect_to survey_money_answers_url, notice: 'Survey answer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_survey_money_answer
      @survey_money_answer = SurveyMoneyAnswer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def survey_money_answer_params
      params.require(:survey_money_answer).permit(:dollar, :survey_money_question_id, :survey_submission_id)
    end
end
