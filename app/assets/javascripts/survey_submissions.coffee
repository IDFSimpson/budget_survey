# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#= require bootstrap-slider

DANGER_PERCENT_COST = 100
WARNING_PERCENT_COST = 90

modifySubmissionSpendingHtml = (sum, max_budget) ->
  percent = ((sum/max_budget) * 100)

  if (percent > DANGER_PERCENT_COST)
    $("#submission_spending_bar .progress-bar").addClass("progress-bar-danger")
  else if (percent > WARNING_PERCENT_COST)
    $("#submission_spending_bar .progress-bar").addClass("progress-bar-warning")
    $("#submission_spending_bar .progress-bar").removeClass("progress-bar-danger")
  else
    $("#submission_spending_bar .progress-bar").removeClass("progress-bar-warning progress-bar-danger")

  $("#submission_spending_bar .progress-bar").attr('aria-valuenow', percent).css('width', percent + '%')
  sum = '$' + sum.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
  $("#submission_spending_value").text(sum)

calculateDollarSum = (inputs) ->
  sum = 0
  inputs.each ->
    sum += parseInt($(this).val())
  return sum

dollarAnswerUpdater = (max_budget) ->
  inputs = $(".dollar_answer:input")
  sum = calculateDollarSum(inputs)
  modifySubmissionSpendingHtml(sum, max_budget)
  inputs.change ->
    sum = calculateDollarSum(inputs)
    modifySubmissionSpendingHtml(sum, max_budget)

affixProgressBar = () ->
  $('#submission_spending_bar').affix({
    offset: {
        top: $('#submission_spending_bar').offset().top
    }
  });

addSliders = () ->
  $(".slider_answer").each ->
    slider_inputs = $(@)
    aSlider = slider_inputs.slider({value: parseInt(slider_inputs.val())})

$(document).on "turbolinks:load", ->
  # max_budget comes from javascript_tag in html

  addSliders()
  dollarAnswerUpdater(max_budget)
  affixProgressBar()
